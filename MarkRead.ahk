﻿SetWorkingDir %A_ScriptDir%

#Include <IE>
#Include <Error>

; Constants
NO_BUTTON_ERROR := ["Can't find button.", "Now closing helper script."]
BTN_SELECTOR = "#btnMarkRead"

; Find the instance
inst := IE.FromExisting("Title")
Error.assert(inst.pwb, "Failed to find open IE session", "Please open Internet Explorer and try again")
inst.WaitReady()

frames := inst.pwb.document.getElementsByTagName("frame")
Error.assert(frames.length, "No frames found", NO_BUTTON_ERROR*)
frames := inst.NodeListToAray(frames)

for i, frame in frames {
	if (!theFrameDocument) 
		theFrameDocument := frame.contentWindow.document
	
	
	button := theFrameDocument.getElementById("btnMarkRead")
	if (button) {
		break
	}
}

Error.assert(button, "Searched all frames.", NO_BUTTON_ERROR*)

button.click()

MsgBox % "The button should be clicked.  Let me know if it worked."
