class Error {

	die(args*) {
		out := ""
		for k, v in args
			out .= v . "`n"
		
		MsgBox % out
		ExitApp 1
	}
	
	break(args*) {
		out := ""
		for k, v in args
			out .= v . "`n"
		
		MsgBox % out
		
		; End the thread, but not the app (unless in root thread)
		Exit
	}
	
	assert(cond, args*) {
		if !cond
			Error.die(args*)
	}
	
}
