﻿SetTitleMatchMode, 2

class IE {

	__New(pwb="") {
		if pwb
			this.pwb := pwb
		else
			pwb := ComObjCreate("InternetExplorer.Application")		
	}

	FromExisting(Name="")		;Retrieve pointer to existing IE window/tab
	{
		IfEqual, Name,, WinGetTitle, Name, ahk_class IEFrame
			Name := ( Name="New Tab - Windows Internet Explorer" ) ? "about:Tabs" 
			: RegExReplace( Name, " - (Windows|Microsoft) Internet Explorer" )
		
		For Pwb in ComObjCreate( "Shell.Application" ).Windows
			If InStr( Pwb.FullName, "iexplore.exe" ) && (Name = "" || InStr(Pwb.LocationName, Name)) {
				Return new IE(Pwb)
			}
			
		return "fail"
	} ;written by Jethrow
	
	select(selector, parent="") {
		if not parent 
			parent := this.pwb.document

		
		if InStr(selector, ".") = 1
			return parent.getElementsByClassName(SubStr(selector, 1))
		if InStr(selector, "#") = 1
			return parent.getElementById(SubStr(selector, 1))
		else
			return parent.getElementsByTagName(selector)
		
	}
		
	WaitReady(Pwb)	;You need to send the IE handle to the function unless you define it as global.
	{
		If !Pwb	;If Pwb is not a valid pointer grab it from the class
			Pwb := this.pwb
		If !Pwb ; If we still don't have one quit
			Return False
		
		Loop	;Otherwise sleep for .1 seconds untill the page starts loading
			Sleep,100
		Until (Pwb.busy)
		
		Loop	;Once it starts loading wait until completes
			Sleep,100
		Until (!Pwb.busy)
		
		Loop	;optional check to wait for the page to completely load
			Sleep,100
		Until (Pwb.Document.Readystate = "Complete")
		
		Return True
	}
	
	NodeListToAray(nodes) {
		If (!nodes)
			return []
		If (!nodes.length)
			return [nodes]
		
		
		Arr := []
		
		
		Loop % nodes.length
			Arr.Insert(nodes[A_Index - 1])
		
		return Arr
	}
}

